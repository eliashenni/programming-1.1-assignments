import java.util.Scanner;
import java.lang.Math;

public class Between{
    public static void main(String[] args) {
        int num1, num2, num3, median;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.print("Enter the first number (1..100): ");
            num1 = sc.nextInt();
        } while(isNumberInRange(num1));

        do {
            System.out.print("Enter the second number (1..100): ");
            num2 = sc.nextInt();
        } while(isNumberInRange(num2));

        do {
            System.out.print("Enter the third number (1..100): ");
            num3 = sc.nextInt();
        } while(isNumberInRange(num3));

        median = Math.max(Math.min(num1,num2), Math.min(Math.max(num1,num2),num3));
        System.out.println("The middle number is: " + median);
    }

    static boolean isNumberInRange(int num) {
        if (num < 1 || num > 100) {
            System.out.println("The number should be between 1 and 100!");
            return true;
        }
        return false;

    }
}
