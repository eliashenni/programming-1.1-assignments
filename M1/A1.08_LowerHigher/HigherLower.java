// You have to import scanner
import java.util.Scanner;

public class HigherLower {
    public static void main(String[] args) {
        //Make new scanner
        Scanner scanner = new Scanner(System.in);
        //Give a solution number
        int solution = 44;

        while (true) { //forever !
            //Ask number and store guess
            System.out.print("Enter a number (1-100): ");
            int guess = scanner.nextInt();
            //Checks guess
            if (guess == solution) {
                System.out.print("Congrats!");
                //Return kills the code
                return;
            } else if (guess < solution) {
                System.out.print("Higher! ");
            } else {
                System.out.print("Lower! ");
            }
        }


    }

}