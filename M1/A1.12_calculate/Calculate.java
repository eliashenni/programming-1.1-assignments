package calculate;

import java.util.Scanner;

public class Calculate {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int a = scanner.nextInt();

        System.out.print("Enter another number: ");
        int b = scanner.nextInt();

        System.out.println("1 = add");
        System.out.println("2 = subtract");
        System.out.println("3 = multiply");
        System.out.println("4 = divide");
        System.out.println("5 = exponentiation");
        System.out.print("Choose an operation: ");
        int c = scanner.nextInt();
        int d;

        if(c == 1) {
            d = a + b;
            System.out.println("Your choice: " + c);
            System.out.print(a + " + " + b + " = " + d);
        }else if(c == 2){
            d = a - b;
            System.out.println("Your choice: " + c);
            System.out.print(a + " - " + b + " = " + d);
        }else if(c == 3) {
            d = a * b;
            System.out.println("Your choice: " + c);
            System.out.print(a + " * " + b + " = " + d);
        }else if(c == 4) {
            d = a / b;
            System.out.println("Your choice: " + c);
            System.out.print(a + " / " + b + " = " + d);
        }else if(c == 5) {
            d = (int) Math.pow(a, b);
            System.out.println("Your choice: " + c);
            System.out.print(a + " ^ " + b + " = " + d);
        }
    }
}
