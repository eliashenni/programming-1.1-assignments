import java.util.Scanner;

public class MultiTables {
    public static void main (String[] args) {
        int mistakes = 0;
        Scanner s = new Scanner(System.in);
        System.out.print("Which multiplication table would you like to see? ");
        int num1 = s.nextInt();

        for (int i = 1; i <= 10; i++) {
            int answer = i * num1;
            System.out.print(i + " * " + num1 + " = ");
            int input = s.nextInt();

            while (!(input == answer)) {
                mistakes++;
                System.out.println("Wrong!");
                System.out.print(i + " * " + num1 + " = ");
                input = s.nextInt();
            }
            System.out.println("Correct!");
        }
        System.out.println("Congrats!!!!!");
        System.out.println("But you made " + mistakes + " mistakes :(");
    }
}
