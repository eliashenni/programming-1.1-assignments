package sequence;

import java.util.Scanner;

public class Sequence {
    public static void main(String[] args) {

        int counter = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("How many numbers do you want to print? ");

        int a = scanner.nextInt();

        System.out.print("What is the starting value? ");

        int b = scanner.nextInt();

        System.out.print("What is the increment? ");

        int c = scanner.nextInt();

        while(counter < a){

            System.out.print(b + " ");

            b = c + b;

            counter ++;
        }


    }
}
