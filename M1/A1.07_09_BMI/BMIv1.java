import java.util.Scanner;

public class BMIv1 {
    public static void main(String[] args) {
        System.out.println("Dear patient, this program will calculate your BMI.");

        Scanner input = new Scanner(System.in);

        System.out.print("Please enter your weight in kilograms: ");
        double weight = input.nextDouble();

        System.out.print("Please enter your length in meters: ");
        double length = input.nextDouble();

        double bmi = weight / (length * length);

        System.out.println("Your BMI is: " + bmi);

            if (bmi < 18){
                System.out.print("You are underweight.");

            }else if (bmi > 18 && bmi < 25){
                System.out.println("You are healthy weight.");

            }else if (bmi >= 25 && bmi < 30){
                System.out.println("You are overweight.");

            }else {
                System.out.println("You are obese.");

            }

    }
}
