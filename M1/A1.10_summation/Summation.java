import java.util.Scanner;

public class Summation {
    public static void main(String[] args) {

        while(true) {
            System.out.print("Enter a number (to stop enter 0): ");

            double input;
            int counter = 1;
            double sum = 0;

            Scanner in = new Scanner(System.in);

            while ((input = in.nextDouble()) != 0) {
                System.out.print("Enter a number (to stop enter 0): ");
                sum = input + sum;
                counter++;
            }

            System.out.println("The sum of these numbers is: " + sum);
            System.out.println("You printed " + counter + " lines.");

            System.out.println("Would you like to continue? y/n");

            String yes = "y";

            Scanner yesno = new Scanner(System.in);
            String answer = yesno.next();

            if (!answer.equals(yes)) {
                System.out.println("Program terminated");
                break;
            }
        }
    }
}
