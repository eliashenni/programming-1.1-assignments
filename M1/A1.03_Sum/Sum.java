import java.util.Scanner;

public class Sum {
    public static void main(String[] args) {
        //New scanner
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number: ");
        //Read users input
        int a;
        a = scanner.nextInt();

        System.out.print("Enter another number: ");
        //Read users input
        int b;
        b = scanner.nextInt();

        //Calculate the sum
        int c;
        c = a + b;
        //Print the sum
        System.out.println("The sum is: " + c);
    }
}
