import java.util.Scanner;

public class Product {
    public static void main(String[] args) {

        Scanner number = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int a = number.nextInt();

        System.out.print("Enter another number: ");
        int b = number.nextInt();

        System.out.print("Enter a final number: ");
        int c = number.nextInt();

        int product = a * b * c;
        System.out.print("The product is: " + product + "!");

    }
}
