import java.time.LocalDate;
import java.util.Scanner;

public class Age {
    public static void main(String[] args) {
        System.out.print("Please enter your name: ");

        Scanner keyboard = new Scanner(System.in);

        String name = keyboard.nextLine();

        System.out.print("Dear " + name + ", please enter the year you were born: ");

        while (true) {

            int year = keyboard.nextInt();

            if (1900 > year || year > 2020) {
                System.out.print("Please enter a real birthdate: ");

            } else {
                int age = LocalDate.now().getYear() - year;
                System.out.println("If all goes well you'll be " + age + " by the end of the year.");
                return;
                }

            }
        }
    }