package a07_palindrome;

import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter a word: ");
        String word = s.next();

        StringBuilder forward = new StringBuilder(word);
        StringBuilder reverse = new StringBuilder(word);
        reverse.reverse();

        if ((reverse.toString()).equals(forward.toString())) {
            System.out.printf("\"%s\" is a palindrome." , word);
        } else {
            System.out.printf("\"%s\" is not a palindrome." , word);
        }
    }
}
