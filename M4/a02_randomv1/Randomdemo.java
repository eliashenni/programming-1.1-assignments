package a02_randomv1;

import java.util.Random;

public class Randomdemo {
    public static void main(String[] args) {
        Random random = new Random();
        
        // dice simulation
        int a = random.nextInt(6) + 1;
        System.out.println(a);

        // random number 10 to 20
        int c = random.nextInt(11) + 10;
        System.out.println(c);

        // any three digit number
        int b = random.nextInt(900) + 100;
        System.out.println(b);

        // seed predetermined
        Random random1 = new Random(10);
        int k = random1.nextInt();
        System.out.println(k);
    }
}
