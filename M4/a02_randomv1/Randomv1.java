package a02_randomv1;

import java.util.Random;

public class Randomv1 {
    public static void main(String[] args) {
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            int dice = random.nextInt(6)+ 1;
            System.out.print(dice + " ");
        }
        System.out.println();
        for (int i = 0; i < 4; i++) {
            boolean test = random.nextBoolean();
            System.out.print(test + " ");
        }
        System.out.println();
        for (int i = 0; i < 3; i++) {
            double decimals = random.nextDouble();
            System.out.printf("%.4f ", decimals);
        }
        System.out.println("\n\n");
        for (int i = 0; i < 10; i++) {
            int test2 = random.nextInt(101)+ 900;
            System.out.print(test2 + " ");
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            int num = random.nextInt(5)+ 5;
            double decimals2 = random.nextDouble();
            System.out.printf("%.2f ", (decimals2 + num));
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            int even;
            do {
                even = random.nextInt(100);
            } while (even % 2 != 0);
            System.out.print(even + " ");
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            int multi;
            do {
                multi = random.nextInt(100);
            } while (multi % 3 != 0);
            System.out.print(multi + " ");
        }
    }
}
