package a10_stringBuilderv3;

import java.util.Scanner;

public class StringBuilderv3 {
    public static void main(String[] args) {
        final int AMOUNT = 5;
        Scanner scanner = new Scanner(System.in);
        StringBuilder builder = new StringBuilder();

        // Read AMOUNT words and append each word to the StringBuilder.
        // Use a for-loop and 'printf'.
        for (int i = 1; i <= AMOUNT; i++) {
            System.out.printf("Enter word %d: ", i);
            builder.append(scanner.next());
            builder.append(" ");
        }
        // Print the content of 'builder'.
        System.out.println();
        System.out.printf("Content of builder: %s\n\n", builder);

        // Create a copy of the StringBuilder object and name it 'copy'. Make sure it contains
        // the same content as the original StringBuilder.
        // Print the content of 'copy'.
        StringBuilder copy = new StringBuilder(builder);
        System.out.printf("Content of copy: %s\n\n", copy);

        // Now check if 'builder' has the same content as 'copy'. Try '==' as well as the 'equals'
        // Note: unlike 'String', 'StringBuilder' doesn't actually have an implementation of the 'equals'
        // method. Yet, we can still call the 'equals' method on objects of type StringBuilder.
        boolean isEqual = (builder == copy);
        boolean isEqual2 = (builder.equals(copy));

        System.out.println("Comparison with == results in: " + isEqual + "\n");
        System.out.println("Comparison with equals results in: " + isEqual2 + "\n");

        // Convert builder to upper case without using the String class and without creating
        // a new StringBuilder.
        // Hint: use an ASCII table.
        for (int i = 0; i < builder.length(); i++) {
            if (builder.charAt(i) != ' ') {
                char c = builder.charAt(i);
                c -= 32;
                builder.setCharAt(i, c);
            }
        }
        System.out.print("Upper case: " + builder);
    }
}
