package a03_randomv2;

import java.util.Random;

public class Randomv2 {
    public static void main(String[] args) {
        Random r = new Random();
        Random random = new Random(42);
        Random random2 = new Random(42);
        for (int i = 0; i < 5; i++) {
            double decimal = r.nextDouble();
            System.out.printf("%.2f ", decimal);
        }
        System.out.println();
        for (int i = 0; i < 10; i++) {
            int num = random.nextInt(42) + 1;
            int num2 = random2.nextInt(42) + 1;
            System.out.print(num + " ");
            System.out.print(num2 + " ");
        }
    }
}
