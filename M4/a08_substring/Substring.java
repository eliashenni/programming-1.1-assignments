package a08_substring;

import java.util.Scanner;

public class Substring {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String line;
        int counter = 0;

        System.out.println("Enter a line of text");
        line = s.nextLine();
        StringBuilder sb1 = new StringBuilder(line);

        for (int i = 0; i < line.length(); i++) {
            if (sb1.charAt(i) == 'o') {
                if (sb1.charAt(i + 1) == 'u') {
                    counter++;
                }
            }
        }
        System.out.println("Substring \"ou\" appears " + counter + " time(s).");
    }
}
