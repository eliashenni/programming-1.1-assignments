package a06_stringBuilderv1;

import java.util.Random;
import java.util.Scanner;
//TODO: this is cool

public class StringBuilderv1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Builder 1
        System.out.print("Name: ");
        String name = scanner.nextLine();

        StringBuilder sb1 = new StringBuilder();
        sb1.append(name.charAt(0));
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (c == ' ') {
                sb1.append(name.charAt(i + 1));
            }
        }
        System.out.println("initials: " + sb1);

        //Builder 2
        StringBuilder sb2 = new StringBuilder(name);
        sb2.reverse();
        System.out.println("Reverse: " + sb2);

        //Builder 3
        StringBuilder sb3 = new StringBuilder(name);
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (c == 'e') {
                sb3.setCharAt(i, 'a');
            }
        }
        System.out.println("Replace e with a: " + sb3);

        //Builder 4
        StringBuilder sb4 = new StringBuilder(name); //'name' is now already in sb4
        StringBuilder sbResult = new StringBuilder();
        Random random = new Random();
        while (sb4.length() > 0) {
            int index = random.nextInt(sb4.length());
            char c = sb4.charAt(index); //takes char to c
            sb4.deleteCharAt(index); //delete char
            sbResult.append(c); //adds c to result
        }
        System.out.println("Random: " + sbResult);
    }
}
