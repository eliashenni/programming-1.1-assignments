package a09_stringBuilderv2;

import java.util.Scanner;

public class StringBuilderv2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        final int MAX = 20;
        int digit;

        //Make sure its a number
        System.out.print("Enter the highest number (1..20): ");
        if (!(s.hasNextInt())) {
            System.out.println("You didn't enter a number!");
            return;
        } else {
            digit = s.nextInt();
        }
        //Make sure its in range
        if (digit < 0 || digit > MAX) {
            System.out.println("The number should be between 1 and 20!");
            return;
        }
        //Make sb1 have all numbers up to 'digit'
        StringBuilder sb1 = new StringBuilder();
        for (int i = 1; i <= digit; i++) {
            sb1.append(i);
            sb1.append(" ");
        }
        System.out.println(sb1);

        //Remove spaces that I added between each number
        for (int i = 0; i < sb1.length(); i++) {
            if (sb1.charAt(i) == ' ') {
                sb1.deleteCharAt(i);
            }
        }
        System.out.print(sb1);
    }
}
