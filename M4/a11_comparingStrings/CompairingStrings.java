package a11_comparingStrings;

import java.util.Scanner;

public class CompairingStrings {
    public static void main(String[] args) {
        //Part 1
        String literal = "Harry";
        String newReference = "Harry";
        String newObject = new String("Harry");

        System.out.println(literal == newReference);
        System.out.println(literal == newObject);
        System.out.println(literal.equals(newReference));
        System.out.println(literal.equals(newObject));

        //Part 2
        Scanner s = new Scanner(System.in);
        System.out.print("First string: ");
        String firstString = s.nextLine();
        System.out.print("Second string: ");
        String secondString = s.nextLine();

        firstString = firstString.trim();
        secondString = secondString.trim();

        int compare = firstString.compareTo(secondString);
        if (compare < 0) {
            System.out.printf("%s %s\n", firstString, secondString);
        } else if (compare > 0) {
            System.out.printf("%s %s\n", secondString, firstString);
        } else {
            System.out.printf("%s %s\n", firstString, secondString);
        }

        //Part 3
        String stringOne = "java";
        String stringTwo = "Java";
        String stringThree = "JAVA";

        if ((stringOne.compareToIgnoreCase(stringTwo)) == 0) {
            System.out.printf("%s and %s are equal\n", stringOne, stringTwo);
        } else {
            System.out.printf("%s and %s are not equal\n", stringOne, stringTwo);
        }
        if ((stringOne.compareToIgnoreCase(stringThree)) == 0) {
            System.out.printf("%s and %s are equal\n", stringOne, stringThree);
        } else {
            System.out.printf("%s and %s are not equal\n", stringOne, stringThree);
        }
        if ((stringTwo.compareToIgnoreCase(stringThree)) == 0) {
            System.out.printf("%s and %s are equal\n", stringTwo, stringThree);
        } else {
            System.out.printf("%s and %s are not equal\n", stringTwo, stringThree);
        }
    }
}
