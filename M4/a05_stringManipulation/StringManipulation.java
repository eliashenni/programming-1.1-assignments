package a05_stringManipulation;

import java.util.Scanner;

public class StringManipulation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a sentence: ");
        String sentence = scanner.nextLine();
        //uppercase
        String upperCaseSentence = sentence.toUpperCase();
        System.out.println("Uppercase: " + upperCaseSentence);
        //lowercase
        String lowerCaseSentence = sentence.toLowerCase();
        System.out.println("Lowercase: " + lowerCaseSentence);
        //replace
        String replacement = sentence.replaceAll("a", "o");
        System.out.println("Replacement ('a' > 'o'): " + replacement);

        /*replace long way
        String replacement = "";
        int sentenceLength = sentence.length(); //or just put sentence.length straight in for loop
        for (int i = 0; i < sentenceLength; i++) {
            char c = sentence.charAt(i);
            //System.out.println("Character at position " + i + " is \'" + c + "\'");
            if (c == 'a') {
                replacement += 'o';
            } else {
                replacement += c;
            }
        }
        System.out.println(replacement);*/

        //total length of sentence
        int sentenceLength = sentence.length();
        System.out.println("Sentence length: " + sentenceLength);

        //first char
        char firstChar = sentence.charAt(0);
        System.out.println("first char: " + firstChar);

        //last char
        char lastChar = sentence.charAt(
                sentence.length() - 1);
        System.out.println("Last char: " + lastChar);

        //amount of times a char is in sentence
        int amount = 0;
        for (int i = 0; i < sentence.length(); i++) {
            int c = sentence.charAt(i);
            if (c == 'e') {
                amount++;
            }
        }
        System.out.println("The letter e was in the sentence " + amount + " times.");
    }
}
