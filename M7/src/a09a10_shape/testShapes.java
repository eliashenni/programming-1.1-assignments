package a09a10_shape;

import a09a10_shape.shapes.Circle;
import a09a10_shape.shapes.Rectangle;
import a09a10_shape.shapes.Shape;
import a09a10_shape.shapes.Square;

public class testShapes {
    public static void main(String[] args) {
        Circle c = new Circle(5);
        Rectangle r = new Rectangle(2,3, 4, 5);
        Square s = new Square(10);
        System.out.println(c.toString());
        System.out.println(r.toString());
        System.out.println(s.toString());
        System.out.println("\nIn an array:");
        Shape[] shapes = new Shape[] {new Rectangle(), new Square(), new Circle()};

        for (Shape i : shapes) {
            System.out.println(i);
        }
    }
}
