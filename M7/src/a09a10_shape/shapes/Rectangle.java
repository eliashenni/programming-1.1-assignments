package a09a10_shape.shapes;

public class Rectangle extends Shape{
    private int height;
    private int width;

    public Rectangle() {
    }

    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public Rectangle(int x, int y, int height, int width) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public Rectangle(int x, int y, int size) {
    }

    public int getHeight() { return height; }
    public int getWidth() { return width; }
    public void setHeight(int height) { this.height = height; }
    public void setWidth(int width) { this.width = width; }

    @Override
    public double getArea() {
        return height*width;
    }
    @Override
    public double getPerimeter() {
        return height+height+width+width;
    }

    @Override
    public String toString() {
        if (this instanceof Square) {
            return super.toString();
        }
        return "Rectangle " + super.toString();
    }
}
