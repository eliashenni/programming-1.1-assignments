package a09a10_shape.shapes;

public abstract class Shape {
    protected int x;
    protected int y;

    public Shape() {
    }

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract double getArea();

    public abstract double getPerimeter();

    @Override
    public String toString() {
        return String.format("at (%d,%d) with perimeter %.2f and surface %.2f", x, y, getPerimeter(), getArea());
    }
}
