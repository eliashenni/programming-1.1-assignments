package a09a10_shape.shapes;

public class Square extends Rectangle{

    public Square() {
    }

    public Square(int size) {
        super(size, size);
    }

    public Square(int x, int y, int size) {
        super(x, y, size);
    }

    public void setSize(int size) {
        setHeight(size);
        setWidth(size);
    }

    @Override
    public double getArea() {
        return getHeight()*getWidth();
    }
    @Override
    public double getPerimeter() {
        return getHeight()+getHeight()+getWidth()+getWidth();
    }

    @Override
    public String toString() {
        return "Square " + super.toString();
    }
}
