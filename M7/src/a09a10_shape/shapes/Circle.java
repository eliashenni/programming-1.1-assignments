package a09a10_shape.shapes;

public class Circle extends Shape{
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    public Circle(int x, int y) {
        super(x, y);
    }

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return  (Math.PI)*(radius*radius);
    }

    @Override
    public double getPerimeter() {
        return  2*Math.PI*radius;
    }

    @Override
    public String toString() {
        return "Circle " + super.toString();
    }
}
