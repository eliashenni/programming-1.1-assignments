package a03_Product;

import a03_Product.products.books.Book;
import a03_Product.products.clothes.Shirt;
import a03_Product.products.electronics.Camera;

public class TestProducts {
    public static void main(String[] args) {
        Book book = new Book
                ("elis book", "elias","2EUH6", "this is a book", 20f);
        Shirt shirt = new Shirt
                ("UBN78", "blue shirt", 40f, "Male", "M");
        Camera camera = new Camera
                ("BHY43", "mirrorless", 200f, 2000);

        System.out.println(book);
        System.out.println(shirt);
        System.out.println(camera);
    }
}
