package a03_Product.products;

public class Product {
    protected String code;
    protected String description;
    protected float price;

    public String getCode() {
        return code;
    }
    public String getDescription() {
        return description;
    }

    public float getPrice(float vat) {
        return price * (1 + vat);
    }
}
