package a03_Product.products.clothes;

import a03_Product.products.Product;

public class Shirt extends Product {
    private String gender;
    private String size;

    public Shirt(String code, String description, float price, String gender, String size) {
        this.code = code;
        this.description = description;
        this.price = price;
        this.gender = gender;
        this.size = size;
    }

    public String getGender() {
        return gender;
    }

    public String getSize() {
        return size;
    }

    public float getVat() {
        return .21f;
    }

    @Override
    public String toString() {
        return String.format("Shirt-- Gender: %s Size: %s Code: %s Description: %s Price: %f"
                , getGender(), getSize(), getCode(), getDescription(), getPrice(getVat()));
    }
}
