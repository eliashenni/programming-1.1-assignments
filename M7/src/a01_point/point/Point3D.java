package a01_point.point;

public class Point3D extends Point{
    private int z;

    public int getZ() {
        return z;
    }

    public Point3D(int x, int y, int z) {
        super(x,y);
        this.z = z;
    }
}
