package a06_productConstructor;

import a06_productConstructor.products.books.Book;
import a06_productConstructor.products.clothes.Shirt;
import a06_productConstructor.products.electronics.Camera;

public class TestProducts {
    public static void main(String[] args) {
        Book book = new Book
                ("elis book", "elias","2EUH6", "this is a book", 20f);
        Shirt shirt = new Shirt
                ("UBN78", "blue shirt", 40f, "Male", "M");
        Camera camera = new Camera
                ("BHY43", "mirrorless", 200f, 2000);

        System.out.println(book);
        System.out.println(shirt);
        System.out.println(camera);
    }
}
