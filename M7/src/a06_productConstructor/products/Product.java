package a06_productConstructor.products;

public class Product {
    private String code;
    private String description;
    private float price;

    public Product(String code, String description, float price) {
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public Product(String code, String description) {
        this (code,description,3f);
    }

    public String getCode() {
        return code;
    }
    public String getDescription() {
        return description;
    }
    public float getVat() {
        return .21f;
    }

    public float getPrice(float vat) {
        return price * (1 + vat);
    }
}
