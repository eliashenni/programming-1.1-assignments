package a06_productConstructor.products.books;

import a06_productConstructor.products.Product;

public class Book extends Product {
    private String title;
    private String author;

    public Book(String title, String author, String code, String description, float price) {
        super(code, description, price);
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public float getVat() {
        return .06f;
    }

    @Override
    public String toString() {
        return String.format("Book-- title: %s Author: %s code: %s description: %s price: %f"
                , getTitle(), getAuthor(), getCode(), getDescription(), getPrice(getVat()));
    }
}
