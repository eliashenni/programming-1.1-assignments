package a06_productConstructor.products.electronics;

import a06_productConstructor.products.Product;

public class Camera extends Product {
    private int pixels;

    public Camera(String code, String description, float price, int pixels) {
        super(code,description,price);
        this.pixels = pixels;
    }

    public int getPixels() {
        return pixels;
    }

    public float getVat() {
        return .21f;
    }

    @Override
    public String toString() {
        return String.format("Camera-- Pixels: %s Code: %s Description: %s Price: %f"
                , getPixels(), getCode(), getDescription(), getPrice(getVat()));
    }
}
