package a05_productEquals.products.clothes;

import a05_productEquals.products.Product;

import java.util.Objects;

public class Shirt extends Product {
    private String gender;
    private String size;

    public Shirt(String code, String description, float price, String gender, String size) {
        this.code = code;
        this.description = description;
        this.price = price;
        this.gender = gender;
        this.size = size;
    }

    public String getGender() {
        return gender;
    }

    public String getSize() {
        return size;
    }

    public float getVat() {
        return .21f;
    }

    @Override
    public String toString() {
        return String.format("Shirt-- Gender: %s Size: %s Code: %s Description: %s Price: %f"
                , getGender(), getSize(), getCode(), getDescription(), getPrice(getVat()));
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o) || !(o instanceof Shirt)) return false;
        Shirt other = (Shirt) o;
        return (Objects.equals(gender, other.gender) &&
                Objects.equals(size, other.size));
    }

    @Override
    public int hashCode() {
        return super.hashCode() ^ (gender == null?0: gender.hashCode())^ (size==null?0:size.hashCode());
    }
}
