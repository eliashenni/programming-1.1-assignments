package a05_productEquals.products.electronics;

import a05_productEquals.products.Product;

import java.util.Objects;

public class Camera extends Product {
    private int pixels;

    public Camera(String code, String description, float price, int pixels) {
        this.code = code;
        this.description = description;
        this.price = price;
        this.pixels = pixels;
    }

    public int getPixels() {
        return pixels;
    }

    public float getVat() {
        return .21f;
    }

    @Override
    public String toString() {
        return String.format("Camera-- Pixels: %s Code: %s Description: %s Price: %f"
                , getPixels(), getCode(), getDescription(), getPrice(getVat()));
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o) || (!(o instanceof Camera))) return false;
        Camera other = (Camera) o;
        return pixels == other.pixels;
    }

    @Override
    public int hashCode() {
        return super.hashCode() ^ pixels;
    }
}
