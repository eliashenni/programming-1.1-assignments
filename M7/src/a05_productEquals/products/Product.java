package a05_productEquals.products;

import java.util.Objects;

public class Product {
    protected String code;
    protected String description;
    protected float price;

    public String getCode() {
        return code;
    }
    public String getDescription() {
        return description;
    }

    public float getPrice(float vat) {
        return price * (1 + vat);
    }

    @Override
    public boolean equals(Object o) {
        if((o == null) || (this.getClass() != o.getClass())) return false;
        Product other = (Product) o;
        return (Objects.equals(code, other.code)) &&
                (Objects.equals(description, other.description)) &&
                (price == other.price);
    }

    @Override
    public int hashCode() {
        return ((code==null?0:code.hashCode())^
                (description==null?0:description.hashCode()) ^
                (int) price);
    }
}
