package a05_productEquals.products.books;

import a05_productEquals.products.Product;

import java.util.Objects;

public class Book extends Product {
    private String title;
    private String author;

    public Book(String title, String author, String code, String description, float price) {
        this.description = description;
        this.code = code;
        this.price = price;
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public float getVat() {
        return .06f;
    }

    @Override
    public String toString() {
        return String.format("Book-- title: %s Author: %s code: %s description: %s price: %f"
                , getTitle(), getAuthor(), getCode(), getDescription(), getPrice(getVat()));
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o) || (!(o instanceof Book))) return false;
        Book other = (Book) o;
        return Objects.equals(title, other.title) &&
                Objects.equals(author, other.author);
    }

    @Override
    public int hashCode() {
        return (super.hashCode() ^ (title == null?0: title.hashCode())
                ^ (author == null?0:author.hashCode()));
    }
}
