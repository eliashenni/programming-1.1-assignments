package a05_productEquals;

import a05_productEquals.products.books.Book;
import a05_productEquals.products.clothes.Shirt;
import a05_productEquals.products.electronics.Camera;

public class TestProducts {
    public static void main(String[] args) {
        Book book = new Book
                ("elis book", "elias","2EUH6", "this is a book", 20f);
        Book book2 = new Book
                ("elis book", "elias","2EUH6", "this is a book", 20f);
        Book book3 = new Book
                ("Ben's book", "elias","2EUH6", "this is a book", 20f);
        Shirt shirt = new Shirt
                ("UBN78", "blue shirt", 40f, "Male", "M");
        Camera camera = new Camera
                ("BHY43", "mirrorless", 200f, 2000);

        System.out.println(book);
        System.out.println("Book 1 and 2 are " + (book.equals(book2)?"the same":"not the same"));
        System.out.println("Book 1 and 3 are " + (book.equals(book3)?"the same":"not the same"));
        System.out.println("Book 1 hash: " + book.hashCode());
        System.out.println("Book 2 hash: " + book2.hashCode());
        System.out.println("Book 3 hash: " + book3.hashCode());

    }
}
