package a13_student.person;

public class Student extends Person{
    private int number;

    public Student(String name, String phone, int number) {
        super(name, phone);
        this.number = number;
    }

    @Override
    public String toString() {
        return String.format("studentNumber: %6d - %s", number, super.toString());
    }
}
