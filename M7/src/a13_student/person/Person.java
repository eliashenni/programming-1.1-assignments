package a13_student.person;

public class Person {
    private String name;
    private Phone phone;

    public Person(String name, String phone) {
        this.phone = new Phone(phone);
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%-10s %-10s", name, phone);
    }


}
