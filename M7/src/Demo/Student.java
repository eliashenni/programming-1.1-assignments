package Demo;

public class Student extends Person {
    private int studentId;
    private int graduatingYear;

    public Student(String name, int age, int studentId, int graduatingYear) {
        this.studentId = studentId;
        this.graduatingYear = graduatingYear;
    }

    public int getStudentId() {
        return studentId;
    }

    public int getGraduatingYear() {
        return graduatingYear;
    }
    public void print() {
        super.print();
        System.out.printf("%d %d", getStudentId(), getGraduatingYear());
    }

    public String toString() {
        return super.toString() + "; Student ID: " + studentId + "; Graduating Year: " + graduatingYear;
    }
}
