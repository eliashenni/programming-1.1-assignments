package Demo;

public class Main {
    public static void main(String[] args) {
        Teacher Lars = new Teacher("Lars Willensens", 20, 123456789, 70000);
        Student eli = new Student("Elias Henni", 20,987654321, 2023);
        eli.setAge(56);

        Lars.print();
        eli.print();

        System.out.println("\nTOSTRING: " + Lars.toString());
        System.out.println("TOSTRING: " + eli);

        Person person = new Teacher("Jan de Rijker", 23, 2345, 2345);

        person.print(); // calling the most specific method

        Person person2 = new Student("12323", 3403, 2323, 233);

        if (person instanceof Teacher) {
            System.out.println("Is a teacher");
        } else if (person instanceof Student) {
            System.out.println("Its a student");
        } else {
            System.out.println("Its not either");
        }
    }
}
