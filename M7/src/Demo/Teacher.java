package Demo;

public class Teacher extends Person { //Teacher is a type of person
    private long teacherId; //string would be fine as well
    private double salary;

    public Teacher(String name, int age, long teacherId, double salary) {
//        this.name = name;
//        this.age = age;
        this.teacherId = teacherId;
        this.salary = salary;
    }

    public long getTeacherId() {
        return teacherId;
    }

    public double getSalary() {
        return salary;
    }
    @Override
    public void print() {
        super.print();
        System.out.printf("%d %.2f%n", getTeacherId(), getSalary());
    }

    public String toString() {
        return super.toString() + "; ID: " + teacherId + "; SALARY: " + salary;
    }
}
