package a02_pointToString.point;

public class Point3D extends Point {
    private int z;

    public int getZ() {
        return z;
    }

    public Point3D(int x, int y, int z) {
        super(x,y);
        this.z = z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public Point3D () {
    }

    @Override
    public String toString() {
        return String.format("%s z: %d", super.toString() , z);
    }
}