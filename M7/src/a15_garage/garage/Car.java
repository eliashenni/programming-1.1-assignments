package a15_garage.garage;

public class Car {
    private Garage garage;
    private String brand;

    public Car(String brand) {
        this.brand = brand;
    }

    public Car(Car car) {
        this.brand = car.brand;
        this.garage = car.garage;
    }

    public Car(String brand, Garage garage) {
        this.garage = garage;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return String.format("Car: %s %s", brand, garage);
    }

    public void setGarage(Garage garage) {
        this.garage = garage;
    }
}
