package a12_shape3D.shapes;

public abstract class Shape3D {
    private String color="black";


    public Shape3D(String color) {
        this.color = color;
    }

    /**
     * Default constructor setting colour black.
     */
    public Shape3D() {
    }


    public String getColor() {
        return color;
    }


    public abstract double surface() ;
    public abstract double volume() ;


    public void display() {
        System.out.format("colour: %-5s surface: %5.3f volume: %5.3f\n", color, surface(), volume());
    }
}