package a12_shape3D.shapes;

public class Cube extends Shape3D{
    private double edge = 1.0;

    public Cube() {
    }

    public Cube(String color, double edge) {
        super(color);
        this.edge = edge;
    }

    public Cube(double edge) {
        this.edge = edge;
    }

    @Override
    public double surface() {
        return 6 * (Math.pow(edge,2));
    }

    @Override
    public double volume() {
        return (Math.pow(edge,3));
    }
}
