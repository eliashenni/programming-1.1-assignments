package a12_shape3D.shapes;

public class Cylinder extends Shape3D{
    private double radius = 1.0;
    private double length = 1.0;

    public Cylinder() {
    }

    public Cylinder(String color, double radius, double length) {
        super(color);
        this.radius = radius;
        this.length = length;
    }

    public Cylinder(double radius, double length) {
        this.radius = radius;
        this.length = length;
    }

    @Override
    public double surface() {
        return (2 * Math.PI * radius * length) + (2 * Math.PI * Math.pow(radius,2));
    }

    @Override
    public double volume() {
        return Math.PI * Math.pow(radius,2) * length;
    }
}
