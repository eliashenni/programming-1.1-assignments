package a12_shape3D.shapes;

public class Sphere extends Shape3D{
    private double radius = 1.0;

    public Sphere() {
    }

    public Sphere(String color, double radius) {
        super(color);
        this.radius = radius;
    }

    public Sphere(double radius) {
        this.radius = radius;
    }

    @Override
    public double surface() {
        return 4 * Math.PI * (Math.pow(radius,2));
    }

    @Override
    public double volume() {
        return (4/3f) * Math.PI * Math.pow(radius,3);
    }
}
