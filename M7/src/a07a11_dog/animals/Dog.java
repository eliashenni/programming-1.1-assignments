package a07a11_dog.animals;

public class Dog extends Animal {

    public Dog(String name, String breed, String color, String chipNumber) {
        super(name, breed, color, chipNumber, "Like a dog in a bowling game");
    }

    @Override
    public String toString() {
        return super.toString() + " with chip " + getChipNumber();
    }
}
