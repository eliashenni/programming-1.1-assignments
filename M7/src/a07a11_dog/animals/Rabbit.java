package a07a11_dog.animals;

public class Rabbit extends Animal {
    private boolean digs;

    public Rabbit(String name, String breed, String color, String chipNumber, boolean digs) {
        super(name, breed, color, chipNumber, "I'm a ice rabbit");
        this.digs = digs;
    }

    @Override
    public String toString() {
        return super.toString() + (digs ? " that digs" : " that doesn't dig");
    }
}
