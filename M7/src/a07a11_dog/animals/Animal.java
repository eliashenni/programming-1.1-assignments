package a07a11_dog.animals;

public class Animal {
    private String name;
    private String breed;
    private String color;
    private final String chipNumber;
    private String tagLine;

    public Animal(String name, String breed, String color, String chipNumber, String tagLine) {
        this.name = name;
        this.breed = breed;
        this.color = color;
        this.chipNumber = chipNumber;
        this.tagLine = tagLine;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%s is a %s %s"
                , name, color, breed);
    }
}
