package a07a11_dog;

import a07a11_dog.animals.Animal;
import a07a11_dog.animals.Dog;
import a07a11_dog.animals.Rabbit;

public class AnimalTest {
    public static void main(String[] args) {
        Dog dog = new Dog
                ("Benny","Beagle", "White and Brown", "1234");
        Rabbit rabbit = new Rabbit
                ("Cinca", "Brittany spaniel", "White and Black", "5678", true);
        Animal animal = new Animal
                ("Eli", "human", "white", "edude", "hello world");
        System.out.println(dog);
        System.out.println(rabbit);
        System.out.println(animal);
        System.out.println();
        System.out.println();

        Animal[] garden = new Animal[3];
        garden[0] = dog;
        garden[1] = rabbit;
        garden[2] = animal;

        String[] names = new String[] {"Sara", "Christel", "Peter"};
        for (int i = 0; i < 3; i++) {
            garden[i].setName(names[i]);
            System.out.println(garden[i]);
        }
    }
}
