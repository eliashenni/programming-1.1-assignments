package a03_animals;

public class Animals {
    private Named[] animals;
    private int amount;

    public Animals() {
        this.animals = new Named[100];
        this.amount = 0;
    }

    public void add(Named named) {
        animals[amount] = named;
        amount++;
    }

    public void print() {
        for (Named n : animals) {
            if (n != null) {
                System.out.println(n);
            }
        }
    }
}
