package a03_animals;

public interface EggLaying {
    int getNumberOfEggsPerYear();
}
