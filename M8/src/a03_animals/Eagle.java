package a03_animals;

public class Eagle implements Named, EggLaying, Flying{
    private String name;
    private int numberOfEggsPerYear;
    private int maxFlyingSpeed;
    private int divingSpeed;

    public Eagle(String name, int numberOfEggsPerYear, int maxFlyingSpeed, int divingSpeed) {
        this.name = name;
        this.numberOfEggsPerYear = numberOfEggsPerYear;
        this.maxFlyingSpeed = maxFlyingSpeed;
        this.divingSpeed = divingSpeed;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getNumberOfEggsPerYear() {
        return numberOfEggsPerYear;
    }

    @Override
    public int getMaxFlyingSpeed() {
        return maxFlyingSpeed;
    }

    public int getDivingSpeed() {
        return divingSpeed;
    }

    @Override
    public String toString() {
        return String.format(
                "Name: %s\n" +
                "Eggs: %d\n" +
                "Speed: %d\n" +
                "Diving speed: %d\n",name,numberOfEggsPerYear,maxFlyingSpeed,divingSpeed);
    }
}
