package a03_animals;

public interface Named {
    String getName();
}
