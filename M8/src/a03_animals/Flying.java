package a03_animals;

public interface Flying {
    int getMaxFlyingSpeed();
}
