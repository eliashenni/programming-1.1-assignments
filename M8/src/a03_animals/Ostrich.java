package a03_animals;

public class Ostrich implements Named, EggLaying{
    private String name;
    private int numberOfEggsPerYear;

    public Ostrich(String name, int numberOfEggsPerYear) {
        this.name = name;
        this.numberOfEggsPerYear = numberOfEggsPerYear;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getNumberOfEggsPerYear() {
        return numberOfEggsPerYear;
    }

    @Override
    public String toString() {
        return String.format("Name: %s\n" +
                "Eggs: %d\n", name,numberOfEggsPerYear);
    }
}
