package a03_animals;

public class Swallow implements Named, EggLaying, Flying{
    private String name;
    private int numberOfEggsPerYear;
    private int maxFlyingSpeed;

    public Swallow(String name, int numberOfEggsPerYear, int maxFlyingSpeed) {
        this.name = name;
        this.numberOfEggsPerYear = numberOfEggsPerYear;
        this.maxFlyingSpeed = maxFlyingSpeed;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getNumberOfEggsPerYear() {
        return numberOfEggsPerYear;
    }

    @Override
    public int getMaxFlyingSpeed() {
        return maxFlyingSpeed;
    }

    @Override
    public String toString() {
        return String.format("Name: %s\n" +
                "Eggs: %d\n" +
                "Speed: %d\n",name,numberOfEggsPerYear,maxFlyingSpeed);
    }
}
