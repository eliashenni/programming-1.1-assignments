package inclass;

public class Plane extends Vehicle implements Wheeled{
    public Plane(int topSpeed) {
        super(topSpeed);
    }

    @Override
    public void print() {
        System.out.println("TOP SPEED`: " + getTopSpeed());
    }

    @Override
    public int getNumberOfWheels() {
        return 3;
    }
}
