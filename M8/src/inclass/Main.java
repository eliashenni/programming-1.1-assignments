package inclass;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car(200, false);
        Car car2 = new Car(220, true);
        Car car3 = new Car(230, false);
        // In memory I will have 3 ints (topSpeeds)
        //     and 3 booleans (isElectric)

        // WHEELS is only in memory 'once'

        System.out.println(car3.isElectric());
        //System.out.println(car3.isElectric); // only when ifElectric is public

        System.out.println(Car.WHEELS);
        //System.out.println(car3.WHEELS);

        System.out.println(Car.hasEngine());
    }

}
