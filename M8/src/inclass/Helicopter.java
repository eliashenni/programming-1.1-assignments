package inclass;

public class Helicopter extends Vehicle {
    private int getVertical;

    public Helicopter (int getVertical, int topSpeed) {
        super(topSpeed);
        this.getVertical = getVertical;
    }

    @Override
    public void print() {
        System.out.println("TOP SPEED`: " + getTopSpeed());
    }
}
