package inclass;

public class Car extends Vehicle implements Wheeled{
    private boolean isElectric;

    // no longer an attribute, now it is tied to the class
    public final static int WHEELS = 4;
    private static final boolean HAS_ENGINE = true;

    public Car(int topSpeed, boolean isElectric) {
        super(topSpeed);
        this.isElectric = isElectric;
    }

    public static boolean hasEngine() {
        // I dont have an instance here, so I
        // can't call instance methods
        return HAS_ENGINE;
    }

    @Override
    public void print() {
        System.out.println("TOP SPEED`: " + getTopSpeed());
        System.out.println("ELECTRIC: " + isElectric);
    }

    public boolean isElectric() {
        return isElectric;
    }

    @Override
    public int getNumberOfWheels() {
        return WHEELS;
    }

    //If abstract, then there is an abstract method in this class
}

