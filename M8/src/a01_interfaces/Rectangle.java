package a01_interfaces;

public class Rectangle extends Shape{
    int width;
    int height;

    public Rectangle(int x, int y, int width, int height) {
        super(x, y);
        setDimensions(width, height);
    }

    public void setDimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
    @Override
    public double getArea() {
        return width*height;
    }
    @Override
    public double getPerimeter() {
        return width + width + height + height;
    }

    @Override
    public void print() {
        System.out.printf("" +
                "Rectangle\n" +
                "=========\n" +
                "Position: (%d, %d)\n" +
                "Width:    %d\n" +
                "Height:   %d\n", getX(), getY(), width, height);
    }
}
