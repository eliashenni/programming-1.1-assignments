package a01_interfaces;

public class Car implements Printable{
    private String Brand;
    private String Model;
    private String licensePlate;

    public Car(String Brand, String Model, String licensePlate) {
        this.Brand = Brand;
        this.Model = Model;
        this.licensePlate = licensePlate;
    }

    @Override
    public void print() {
        System.out.printf(
                "Car\n" +
                "===\n" +
                "Brand:         %s\n" +
                "Model:         %s\n" +
                "License plate: %s\n", Brand, Model, licensePlate);
    }
}
