package a07_division;

import java.util.Scanner;

public class Division {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a value for 'a': ");
        int a = scanner.nextInt();
        System.out.print("Please enter a value for 'b': ");
        int b = scanner.nextInt();

        try {
            System.out.printf("a / b = %d", a/b);
        } catch (ArithmeticException e) {
            System.out.println("Can't divide by at zero!");
        }
    }
}
