package a04_a05_points;

public class Point {
    private int x;
    private int y;
    private final static String COLOR = "RED";
    private static int count = 0;
    // every attribute is initialized to a null or 0

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
        count++;
    }

    public static int getCount() {
        return count;
    }

    public static String getCOLOR() {
        return COLOR;
    }

    public Point() {
        this(0,0);
    }

    public int getX() {
        return x;
    }

    @Override
    public String toString() {
        return "Point: (" + x + "," + y +
                ") Colorlength = " + COLOR.length();
    }

    public int getY() {
        return y;
    }
}
