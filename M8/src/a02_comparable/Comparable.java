package a02_comparable;

public interface Comparable {
    boolean isEqualTo(Object o);
    boolean isGreaterThan(Object o);
    boolean isLessThan(Object o);
}
