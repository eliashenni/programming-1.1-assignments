package a06_input;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        float number = 0.0f;
        boolean isValidNumber = false;

        do {
            System.out.print("Please enter a decimal number between 0 and 10: ");
            try {
                number = scanner.nextFloat();
                if (number > 0.0 && number < 10.0) {
                    isValidNumber = true;
                } else {
                    System.out.println("That number is not between 0 and 10!");
                }
            } catch (InputMismatchException e) {
                System.out.println("That's not a decimal number!");
                scanner.nextLine();
            }
        } while (!isValidNumber);

        System.out.print("\nThanks, " + number + " is a valid number!");
    }
}
