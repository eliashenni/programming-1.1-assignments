package a08_rockPaperScissors;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        final String[] options = new String[] {"rock", "paper", "scissors"};
        boolean gameFinished = false;
        boolean validInput = false;
        int userInput = 0;
        Random random = new Random();

        do {
            System.out.print("Rock(1), paper(2) or scissors(3)? (enter 0 to stop): ");
            do {
                try {
                    userInput = scanner.nextInt();
                    if (userInput == 0) {
                        validInput = true;
                        gameFinished = true;
                    } else if (userInput >= 1 && userInput <= 3) {
                        validInput = true;
                    } else {
                        System.out.print("Enter 0, 1, 2 or 3: ");
                        validInput = false;
                    }
                } catch (InputMismatchException e) {
                    System.out.print("Enter 0, 1, 2 or 3: ");
                    scanner.nextLine();
                    validInput = false;
                }
            } while(!validInput);

            if (userInput != 0) {
                int userChoice = userInput - 1;
                int comChoice = random.nextInt(3);
                System.out.println("Your choice: " + options[userChoice]);
                System.out.println("My choice: " + options[comChoice]);

                if (userChoice == comChoice) {
                    System.out.println("\t\tIt's a tie!\n");
                } else if ((userChoice == 0 && comChoice == 2) ||
                        (userChoice == 1 && comChoice == 0) ||
                        (userChoice == 2 && comChoice == 1)) {
                    System.out.println("\t\tYou won, congratulations!\n");
                } else if ((comChoice == 0 && userChoice == 2) ||
                        (comChoice == 1 && userChoice == 0) ||
                        (comChoice == 2 && userChoice == 1)) {
                    System.out.println("\t\tI won!\n");
                }
            }
        } while (!gameFinished);
        System.out.println("Game ended");
    }
}
