package a08_split;

public class Split {
    public static void main(String[] args) {
        String text = "Java can be tricky at times";

        String[] split = text.split(" ");

        for (String i : split) {
            System.out.print("\"" + i + "\" ");
        }
    }
}
