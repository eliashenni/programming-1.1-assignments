package a06_countingLetters;

import java.util.Scanner;

public class CountingLetters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] letters = new int[26];
        int total = 0;
        System.out.print("Please enter a sentence: ");
        String sentence = scanner.nextLine().toLowerCase();

        for (int i = 0; i < sentence.length(); i++) {
            int a = sentence.charAt(i);
            if (a >= 97 && a <= 122) {
                int diff = a - 97;
                letters[diff] += 1;
                total++;
            }
        }
        System.out.print("Letter frequencies: ");
        for (int i = 0; i < 26; i++) {
            if (i % 4 == 0) {
                System.out.println();
            }
            char e = (char)(i + 97);
            System.out.printf("%s --> %d times  ", e , letters[i]);
        }
        System.out.println("\nTotal amount of letters: " + total);
    }
}

