package a03_forEach;

public class ForEach {
    public static void main(String[] args) {
        //Part1
        System.out.print("Original: ");
        int[] lotteryNumbers = {3, 6, 17, 31, 32, 43};
        for (int number : lotteryNumbers) {
            System.out.print(number + " ");
        }
        System.out.println();
        System.out.print("Replace: ");
        lotteryNumbers[1] = 13;
        for (int number : lotteryNumbers) {
            System.out.print(number + " ");
        }
        System.out.println();

        //Part 2
        System.out.print("Multiples: ");
        int[] multiples = new int[20];
        for (int i = 0; i < multiples.length; i++) {
            multiples[i] = (7 * i) + 7;
        }
        for(int i : multiples) {
            System.out.print(i + " ");
        }
        System.out.println();

        //Part 3
        System.out.print("Reverse: ");
        for (int i = (multiples.length) - 1; i >= 0; i--) {
            System.out.print(multiples[i] + " ");
        }
    }
}
