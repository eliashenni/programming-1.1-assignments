package a10_playingCards;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isInRange = false;
        int cardCount;

        String[] suits = {
                "clubs", "diamonds", "spades", "hearts"
        };
        String[] ranks = {
                "ace", "two", "three", "four", "five",
                "six", "seven", "eight", "nine", "ten",
                "jack", "queen", "king"
        };

        Card[] deck = new Card[52];

        int deckPlace = 0;
        for (String suit : suits) {
            for (String rank : ranks) {
                deck[deckPlace] = new Card(rank, suit);
                deckPlace++;
            }
        }


        do {
            System.out.print("How many cards would you like? (1..5) ");
            cardCount = scanner.nextInt();
            if (cardCount < 1 || cardCount > 5) {
                System.out.println("That's not a valid amount!");
            } else {
                isInRange = true;
            }
        } while (!(isInRange));


        Card[] selectedCards = new Card[cardCount];
        Random random = new Random();
        int[] uniqueCard = new int[5];
        boolean isUnique = false;

        for (int i = 0; i < selectedCards.length; i++) {
            int newCard = random.nextInt(deck.length);
            do {
                if (newCard == uniqueCard[0] || newCard == uniqueCard[1] || newCard == uniqueCard[2]
                        || newCard == uniqueCard[3] || newCard == uniqueCard[4]) {
                    newCard = random.nextInt(52);
                } else {
                    isUnique = true;
                }
            } while (!(isUnique));

            uniqueCard[i] = newCard;
            selectedCards[i] = deck[newCard];
        }

        for (Card i : selectedCards) {
            System.out.println(i);
        }
    }
}
