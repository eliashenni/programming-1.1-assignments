package a05_temperatures;

import java.util.Scanner;

public class Temperatures {
    public static void main(String[] args) {
        int counter = 0;
        double average = 0;
        double[] temperatures = new double[3];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter 3 temperatures: ");

        for (int i = 0; i < temperatures.length; i ++) {
            System.out.print("Day " + (i + 1) + ": ");
            temperatures[i] = scanner.nextDouble();
            average += temperatures[i];
        }
        System.out.println("\nSummary:\n" +
                "================");
        for (double i : temperatures) {
            System.out.printf("Day %d\t\t%.1f\n", (counter + 1), i);
            counter++;
        }
        average = average / 3;
        System.out.println("================");
        System.out.printf("Average: %.2f", average);
    }
}
