package a09_arrayOfCharacters;

public class ArrayOfCharacters {
    public static void main(String[] args) {
        String word = "JavaScript";

        char[] letters = word.toCharArray();

        for (char i : letters) {
            System.out.print(i + " ");
        }
    }
}
