package a12_parkingGarage;

import java.util.Random;

public class ParkingGarage {
    public static void main(String[] args) {
        Random random = new Random();
        int [][][] parkingSpots = new int[2][2][15];

        for (int[][] singleGarage : parkingSpots) {
            for (int [] singleFloor : singleGarage) {
                for (int singleSpot : singleFloor) {
                    singleFloor[singleSpot] = random.nextInt(10);
                }
            }
        }

        for (int garage = 0 ; garage < parkingSpots.length; garage++) {
            int sum = 0;
            for (int[] floor : parkingSpots[garage]) {
                for (int singleSpot : floor) {
                    sum += floor[singleSpot];
                }
            }
            System.out.printf("Garage %d: used %d times\n", garage + 1, sum);
        }
    }
}
