package inclass;

import java.util.Random;
import java.util.Scanner;

public class practice {
    public static void main(String[] args) {
        //Introduction
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter how many numbers you'd like to enter: ");
        int numberCount = scanner.nextInt();

        // 2. Creation
        int[] numbers = new int [numberCount];
        int[] numbers2;

        double[] temperatures = new double[5];
        boolean[] coinTosses = {true, false, true, false};
        double[] temperatures2 = temperatures;


        // 3. Usage
        /*numbers[0] = 42;
        numbers[1] = 12;
        numbers[2] = 22;
        //...
        numbers[9] = 100;*/
        //counts from 0 up
        String s = "Eli";
        System.out.println(s.charAt(2));

        // 4. For-each loop

        for (int i = 0; i < numberCount; i++) {
            System.out.print("Enter a number: ");
            numbers[i] = scanner.nextInt();
        }

        Random random = new Random();
        for (int i = 0; i < coinTosses.length; i++) {
            coinTosses[i] = random.nextBoolean();
        }
        System.out.println();

        //how you print everything
        for (double t : temperatures) {
            System.out.println(t);
        }
        // 5. Array of objects
        String[] words = {"This", "is", "an", "array"};
        StringBuilder[] builders = {
                new StringBuilder(),
                new StringBuilder("HEY"),
                new StringBuilder("0000")
        };
        Random[] randoms = new Random[10];
        builders[1].append(" there!");
        System.out.println(builders[1]);

        System.out.println("First word: " + words[0]);

        // 6. Arrays of arrays


    }
}
