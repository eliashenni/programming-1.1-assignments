package a01_Arraysv1;

public class Arraysv1 {
    public static void main(String[] args) {
        int[] numbers = new int[5];
        float[] stockMarketRates = new float[20];
        boolean[] switches = new boolean[8];
        String[] words = new String[4];

        System.out.println("First of int: " + numbers[0]);
        System.out.println("First of float: " + stockMarketRates[0]);
        System.out.println("First of boolean: " + switches[0]);
        System.out.println("First of string: " + words[0]);
        System.out.println();
        System.out.println("Last of int: " + numbers[4]);
        System.out.println("Last of float: " + stockMarketRates[19]);
        System.out.println("Last of boolean: " + switches[7]);
        System.out.println("Last of string: " + words[3]);
    }
}
