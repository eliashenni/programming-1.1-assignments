package a10_Digitsv1;
import java.util.Scanner;

public class Digitsv1 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        boolean shouldIContinue = true;
        int firstNum,secondNum,thirdNum,forthNum,number;

        while (shouldIContinue) {
        System.out.println("Enter four digits (0..9) (-1 to quit)");
        System.out.print("The first digit: ");
        firstNum = keyboard.nextInt();
            if (firstNum == -1) {
                shouldIContinue = false;
            } else {
                System.out.print("The second digit: ");
                secondNum = keyboard.nextInt();
                System.out.print("The third digit: ");
                thirdNum = keyboard.nextInt();
                System.out.print("The forth digit: ");
                forthNum = keyboard.nextInt();
                if (firstNum < 0 || secondNum < 0 ||thirdNum < 0 ||forthNum < 0 || firstNum > 9 || secondNum > 9 || thirdNum > 9 || forthNum > 9) {
                    System.out.println("Make sure each digit lies in the range [0-9]!");
                } else {
                    number = firstNum * 1000 + secondNum * 100 + thirdNum * 10 + forthNum;
                    System.out.println("The number is: " + number);
                }
            }
        }
    }
}
