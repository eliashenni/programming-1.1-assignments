package a12_Counting;

public class Counting {
    public static void main(String[] args) {
        final int MAX = 10;
        int count = 1;
        int reverse = 10;
        while (!(count > MAX)) System.out.println((count++) + " - " + (reverse--));
    }
}
