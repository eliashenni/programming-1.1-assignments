package a05_NumbersV3;
import java.util.Scanner;

public class NumbersV3 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        long MINIMUM_DIVIDEND = 1_000_000_000_000L;
        long MINIMUM_DIVISOR = 10_000_000L;

        System.out.print("Enter a 13-digit whole number: ");
        long dividend = s.nextLong();
        if (dividend < MINIMUM_DIVIDEND) {
            System.out.println("This number is too small.");
            return;
        }

        System.out.print("Enter an 8-digit whole number: ");
        long divisor = s.nextLong();
        if (divisor < MINIMUM_DIVISOR) {
            System.out.println("This number is too small.");
            return;
        }
        double quotient = (double)dividend / (double)divisor;
        System.out.println("The quotient is " + quotient);

        System.out.println("Without the fractional part it's " + (int)quotient);
    }
}
