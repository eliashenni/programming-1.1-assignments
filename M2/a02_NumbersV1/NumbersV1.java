package a02_NumbersV1;

public class NumbersV1 {
    public static void main(String[] args) {
        //Part one
        int a = 2_000_000_000;
        int b = 2_000_000_000;
        System.out.println((long)a + b);

        //Part two
        long c = 10_000;
        long d = 10_000;
        int result = (int) (c + d);
        System.out.println(result);

        //Part three
        int first = 8;
        int second = 5;
        System.out.println(first + second);
        System.out.println(first - second);
        System.out.println(first * second);
        System.out.println(first / second);
        System.out.println(first % second);

        //part four
        //++ or -- before a var makes it increase first, after means after
        int resultTwo;
        resultTwo = ++first;
        System.out.println(resultTwo);
        resultTwo = first++;
        System.out.println(resultTwo);
        resultTwo = --second;
        System.out.println(resultTwo);
        resultTwo = second--;
        System.out.println(resultTwo);
    }
}
