package week2;

import java.util.Scanner;

public class assignment {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int sum = 0; //only once

        while (true) {
            System.out.print("Enter a number (enter 0 to stop): ");
            int number = scanner.nextInt();
            sum = sum + number;

            if (number == 0) {
                System.out.print("The sum of these numbers is: " + sum);
                return;
            }
        }

    }
}
