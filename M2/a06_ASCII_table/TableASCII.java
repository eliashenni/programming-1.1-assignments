package a06_ASCII_table;

public class TableASCII {
    public static void main(String[] args) {
        for (char i = 32; i <= 255; i++) {
            System.out.printf("    %c (%3d)", i, (int) i);

            if (i % 6 == 1) {
                System.out.println();
            }
        }
    }
}
