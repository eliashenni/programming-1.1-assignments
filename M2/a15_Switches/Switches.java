package a15_Switches;

import java.util.Scanner;

public class Switches {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        boolean s1, s2, s3;
        int counter = 0;
        System.out.print("Enter the state of switch 1 (true or false): ");
        s1 = s.nextBoolean();
        System.out.print("Enter the state of switch 2 (true or false): ");
        s2 = s.nextBoolean();
        System.out.print("Enter the state of switch 3 (true or false): ");
        s3 = s.nextBoolean();

        if (s1) { counter++;}
        if (s2) { counter++;}
        if (s3) { counter++;}

        switch (counter) {
            case 0 : System.out.println("No switches are turned on"); break;
            case 1 : System.out.println("At most one switch is turned on."); break;
            case 2 : System.out.println("At least two switches are turned on.\n" +
                    "Exactly two switches are turned on."); break;
            case 3 : System.out.println("At least two switches are turned on."); break;
        }
    }
}
