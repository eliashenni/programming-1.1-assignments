package a07_ASCII_sum;
import java.util.Scanner;
/* Write a program that calculates the sum of the decimal codes of
each character in a given string of text (including spaces and other "special" characters).

Enter a string of text: The quick brown fox jumps over the lazy dog
sum = 4057
*/
public class SumASCII {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a string of text: ");
        String line = keyboard.nextLine();
        int sum = 0;
        for (char i : line.toCharArray()) {
            sum += i;
        }
        System.out.println("sum: " + sum);
    }
}
