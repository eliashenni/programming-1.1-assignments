package a16_scrambleName;

import java.util.Random;
import java.util.Scanner;

public class ScrambleName {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter you name: ");
        String name = s.nextLine();
        Random random = new Random();
        String nameCopy = name;
        String nameScrambled = "";
        //while name copy still has contents
        while (!nameCopy.isEmpty()) {
            //picks out a random index of nameCopy
            int positionChar = random.nextInt(nameCopy.length());
            //c equals the char at index
            char c = nameCopy.charAt(positionChar);
            //takes out any spaces
            if (c != ' ') {
                //appends c to scrambled name
                nameScrambled += c;
            }
            //adds all chars up to positionChar + all chars after position char
            nameCopy = nameCopy.substring(0, positionChar) + nameCopy.substring(positionChar + 1);
        }
        System.out.printf("Hi %s, your scrambled name is %s", name, nameScrambled);
    }
}
