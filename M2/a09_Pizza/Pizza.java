package a09_Pizza;
import java.util.Scanner;

public class Pizza {
    public static void main(String[] args) {
        final int costPizza = 800;
        final int costTopping = 50;
        int total = 0;
        int count = 1;
        int numPizza;
        int numToppings;

        Scanner s = new Scanner(System.in);
        System.out.print("How many pizzas would you like: ");
        numPizza = s.nextInt();
        total += numPizza * costPizza;

        while (count <= numPizza) {
            System.out.print("How many extra toppings for pizza " + count + " : ");
            numToppings = s.nextInt();
            total += numToppings * costTopping;
            count++;
        }
        System.out.printf("Total price: €%.1f\n", total / 100.0);
    }
}
