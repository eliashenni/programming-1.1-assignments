package a14_Encoding;

import java.util.Scanner;

public class Encoding {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter a message: ");
        String message = s.nextLine().toUpperCase();
        System.out.print("Displacement to be used: ");
        int displacement = s.nextInt();
        String encoded = "";

        for (char c : message.toCharArray()) {
            if (c < 65 || c > 90) {
                encoded += c;
            } else {
                encoded += (char) ((c - 65 + displacement) % 26 + 65);
            }
        }
        System.out.println(encoded);

        String decoded = "";
        for (char c : encoded.toCharArray()) {
            if (c < 65 || c > 90) {
                decoded += c;
            } else {
                decoded += (char)((c - 65 - displacement + 26) % 26 + 65);
            }
        }
        System.out.println(decoded);
    }
}
