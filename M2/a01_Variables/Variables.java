package a01_Variables;

public class Variables {
    public static void main(String[] args) {
        boolean a = true;
        char b = 'A';
        byte c = 127;
        short d = 32_767;
        int e = 233_232_323;
        long f = 232_323_232;
        float g = 34.33f;
        double h = 32.34;
        final double PI = 3.14;
        // PI = 23.5 does not work because its 'final'

        System.out.println("A: " + a);
        System.out.println("B: " + b);
        System.out.println("C: " + c);
        System.out.println("D: " + d);
        System.out.println("E: " + e);
        System.out.println("F: " + f);
        System.out.println("G: " + g);
        System.out.println("H: " + h);
        System.out.println("PI: " + PI);


    }
}
