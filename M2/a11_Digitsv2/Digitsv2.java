package a11_Digitsv2;
import java.util.Scanner;

public class Digitsv2 {
    public static void main( String[] args) {
        final int MINIMUM = 1000;
        final int MAXIMUM = 9999;
        int number;
        int sum = 0;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter a 4-digit whole number (1000..9999): ");
        number = s.nextInt();
        if (number < MINIMUM || number > MAXIMUM) {
            System.out.println("Number is out of range!");
        } else {
            sum += number % 10;
            number = number / 10;

            sum += number % 10;
            number = number / 10;

            sum += number % 10;
            number = number / 10;

            sum += number % 10;
            System.out.println("The sum is: " + sum);
        }
    }
}
