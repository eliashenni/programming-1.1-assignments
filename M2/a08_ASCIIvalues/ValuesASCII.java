package a08_ASCIIvalues;
import java.util.Scanner;

public class ValuesASCII {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a string: ");
        String line = keyboard.nextLine();

        int value = 0;
        for (char i : line.toCharArray()) {
            value += i;
            System.out.println(i + " has an ASCII value of " + value);
            value = 0;
        }
    }
}
