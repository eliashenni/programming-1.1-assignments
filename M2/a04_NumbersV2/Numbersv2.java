package a04_NumbersV2;
import java.util.Scanner;

public class Numbersv2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        long MINIMUM = 100_000;
        long MAXIMUM = 999_999;

        System.out.print("Enter a 6-digit whole number: ");
        long num1 = s.nextInt();
        System.out.print("Enter another 6-digit whole number: ");
        long num2 = s.nextInt();

        if ((num1 < MINIMUM || num2 < MINIMUM)) {
            System.out.println("One of the numbers is too small.");
        } else if((num1 > MAXIMUM || num2 > MAXIMUM)) {
            System.out.println("One of the numbers is too large.");
        } else {
            long sum = num1 * num2;
            System.out.println("The product is: " + sum);
            long remain = sum % 100000;
            System.out.println("The 5 final digits are: " + remain);
        }
    }
}
