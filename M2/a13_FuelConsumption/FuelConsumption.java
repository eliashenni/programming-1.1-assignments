package a13_FuelConsumption;
import java.util.Scanner;
/*
Enter the previous mileage: 79114
Enter the current mileage: 80103
Enter the amount of liters refilled: 60.4
Comsumption for 989km driven: 6.11 liters/100km
 */

public class FuelConsumption {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int lastMile, mileage, miles;
        double fuel, litersPer;
        System.out.print("Enter the previous mileage: ");
        lastMile = s.nextInt();
        System.out.print("Enter the current mileage: ");
        mileage = s.nextInt();
        System.out.print("Enter the amount of liters refilled: ");
        fuel = s.nextDouble();

        miles = mileage - lastMile;
        litersPer = fuel * 100 / miles;
        System.out.printf("Comsumption for %dkm driven: %.2f liters/100km", miles, litersPer);
    }
}
