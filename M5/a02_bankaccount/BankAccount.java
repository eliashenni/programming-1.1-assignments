package a02_bankaccount;

public class BankAccount {
    String holder;
    String iban;
    double balance;
    //constructor that initializes all attributes
    public BankAccount(String holder, String iban, double initialBalance) {
        this(holder, iban);
        this.balance = initialBalance;
    }
    //constructor with only holder and iban. Balance should be 0.
    public BankAccount(String holder, String iban) {
        this.holder = holder;
        this.iban = iban;
        balance = 0.0;
    }

    public String getHolder() {
        return holder;
    }

    public String getIban() {
        return iban;
    }

    public double getBalance() {
        return balance;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void deposit(double amount) {
        balance += amount;
    }
    public boolean withdraw(double amount) {
        if (balance - amount > 0) {
            this.balance -= amount;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return String.format("The account %s of %s has a balance of € %.2f", iban, holder, balance);
    }
}
