package a01_book;

public class Book {
    //add attributes
    String author;
    String title;
    int pages;
    boolean onLoan;
    //add constructor with author, title, and pages as parameters
    public Book(String author, String title, int pages) {
        this.author = author;
        this.title = title;
        this.pages = pages;
    }
    //add constructor without parameters and call the constructor you wrote earlier
    public Book() {
        this("unknown", "unknown", 0);
    }
    //getters and setters for all attributes
    public String getAuthor() {
        return author;
    }
    public String getTitle() {
        return title;
    }
    public int getPages() {
        return pages;
    }
    public boolean isOnLoan() {
        return onLoan;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setPages(int pages) {
        this.pages = pages;
    }
    public void setOnLoan(boolean onLoan) {
        this.onLoan = onLoan;
    }

    public String toString() {
        return "Book " + title.toUpperCase() +
                "(" + pages + " pages), written by " + author.toUpperCase() +
                " is " + (onLoan ? "on loan." : "available.");
    }
}
