package a01_book;

public class TestBook {
    public static void main(String[] args) {
        Book b1 = new Book("deitel & deitel", "java, how to program", 1186);
        Book b2 = new Book("Hillary mantel", "Wolf Hall", 604);
        Book b3 = new Book("P.G. Wodehouse", "Leave it to Psmith", 352);

        b1.setOnLoan(true);
        b3.setOnLoan(true);

        System.out.println(b1.toString());
        System.out.println(b2.toString());
        System.out.println(b3.toString());
    }
}
