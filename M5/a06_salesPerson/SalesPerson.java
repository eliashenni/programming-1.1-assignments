package a06_salesPerson;

public class SalesPerson {
    private String name;
    private double revenue;

    public SalesPerson(String name, double revenue) {
        this.name = name;
        this.revenue = revenue;
    }
    //it will be true if salesPerson before the method has more revenue
    //it will be false is salesPerson other has more revenue
    public boolean hasMoreRevenue(SalesPerson other) {
        return revenue > other.getRevenue();
    }

    public String getName() {
        return name;
    }
    public double getRevenue() {
        return revenue;
    }
}
