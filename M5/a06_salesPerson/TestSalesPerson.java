package a06_salesPerson;

import java.util.Scanner;

public class TestSalesPerson {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String name;
        double revenue ;

        name="Jan";
        System.out.printf("Enter %s's revenue: ",name);
        SalesPerson one = new SalesPerson(name, scanner.nextDouble());

        name="Laetitia";
        System.out.printf("Enter %s's revenue: ",name);
        SalesPerson two = new SalesPerson(name, scanner.nextDouble());

        name="Lotte";
        System.out.printf("Enter %s's revenue: ",name);
        SalesPerson three = new SalesPerson(name, scanner.nextDouble());
        // TODO: Print out the name of the top earner!
        //new salesperson
        SalesPerson top;
        //top = if one has more revenue then two, then one equals top, else two equals top
        top = one.hasMoreRevenue(two)?one:two;
        //top = if (from the last one) has more revenue then three, then top equals top, else three equals top
        top = top.hasMoreRevenue(three)?top:three;
        //get name of top to print
        System.out.printf("Our top earneris %s!\n", top.getName());
    }
}