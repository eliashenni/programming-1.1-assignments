package a04_operations;

public class Operations {
    public int numberOne;
    public int numberTwo;

    public Operations(int numberOne, int numberTwo) {
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
    }

    public int sum() {
        return numberOne + numberTwo;
    }

    public int difference() {
        return numberOne - numberTwo;
    }

    public int product() {
        return numberOne * numberTwo;
    }

    public double quotient() {
        return (double) numberOne / numberTwo;
    }

    @Override
    public String toString() {
        return String.format("The sum is " + sum() +
                "\nThe difference is " + difference() +
                "\nThe product is " + product() +
                "\nThe quotient is %.2f\n", quotient());
    }
}
