package a04_operations;

import java.util.Scanner;

public class DemoOperations {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Please enter the first integer: ");
        int firstNum = s.nextInt();
        System.out.print("Please enter the second integer: ");
        int secondNum = s.nextInt();

        Operations demo = new Operations(firstNum, secondNum);
        System.out.println(demo.toString());
    }
}
