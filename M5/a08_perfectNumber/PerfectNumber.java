package a08_perfectNumber;

public class PerfectNumber {
    StringBuilder sb = new StringBuilder();
    int sum = 0;

    public String getPerfect(int number) {
        sum = 0;
        sb.delete(0, 10000);
        for (int i = 1; i <= number / 2; i++) {
            if (number % i == 0) {
                sum = i + sum;
                sb.append(" ");
                sb.append(i);
            }
        }
        if (sum == number) {
            return sum + " --> " + sb;
        } else {
            return null;
        }
    }
}
