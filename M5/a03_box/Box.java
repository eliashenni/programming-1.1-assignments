package a03_box;

public class Box {
    String type;
    double length;
    double width;
    double height;

    public Box (String type, double length, double width, double height) {
        this.type = type;
        this.length = length;
        this.width = width;
        this.height = height;
    }
    public Box (String type, double length) {
        this.type = type;
        this.length = length;
        this.width = length;
        this.height = length;
    }

    public double surface() {
        return ((length * height) * 2) + ((height * width)* 2) + ((length * width) * 2);
    }

    public double volume() {
        return length * width * height;
    }

    public double tapeLength() {
        return Math.min((length * 2) + (height * 2), (width * 2) + (height * 2));
    }

    @Override
    public String toString() {
        return String.format(
                "Type: %s\n" +
                "Length: %.1fcm\n" +
                "Width: %.1fcm\n" +
                "Height: %.1fcm\n" +
                "Surface: %.1fcm²\n" +
                "Volume: %.1fcm³\n" +
                "Minimum tapelength: %.1fcm\n", type, length, width, height, surface(), volume(), tapeLength());
    }
}
