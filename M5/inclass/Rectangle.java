package inclass;

public class Rectangle {
    private int x;
    private int y;
    private int width;
    private int height;

    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    //where nothing is filled in
    public Rectangle() {
        this(1,1,1,1); //must be first
        /*x = 1;
        y = 1;
        width = 1;
        height = 1;*/
    }

    public int surface() {
        return width * height;
    }
    public void setX(int x) { this.x = x; }

    public int getX() {
        return x;
    }
    public void setY(int y) { this.y = y; }

    public int getY() {
        return y;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {return width; }

}