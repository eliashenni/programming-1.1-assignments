package a05_maximum;

public class Maximum {
    double one;
    double two;
    double three;

    public Maximum() {
        System.out.println("constructor without parameters called");
    }

    public Maximum (int one, int two, int three) {
        this.one = one;
        this.two = two;
        this.three = three;
        System.out.println("int constructor called");
    }

    public Maximum (long one, long two, long three) {
        this.one = one;
        this.two = two;
        this.three = three;
        System.out.println("long constructor called");
    }

    public Maximum (double one, double two, double three) {
        this.one = one;
        this.two = two;
        this.three = three;
        System.out.println("double constructor called");
    }

    public double max() {
        return Math.max(three, Math.max(one, two));
    }

    public double max(int one, int two, int three) {
        System.out.println("int parameters method called");
        return Math.max(three, Math.max(one, two));
    }

    public double max(long one, long two, long three) {
        System.out.println("long parameters method called");
        return Math.max(three, Math.max(one, two));
    }

    public double max(double one, double two, double three) {
        System.out.println("double parameters method called");
        return Math.max(three, Math.max(one, two));
    }
}
