package a07_circle;

public class Circle {
    private int radius;
    private String color = "black";

    public Circle(int radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    public double circumference() {
        return 2 * Math.PI * radius;
    }

    public double surface() {
        return Math.PI * (radius * radius);
    }

    //public double deltaCircumference(Circle other) {
    //       return Math.abs(this.circumference() - other.circumference());
    //    }
    public double deltaCircumference(Circle other) {
        if (this.circumference() > other.circumference()) {
            return this.circumference() - other.circumference();
        } else {
            return other.circumference() - this.circumference();
        }
    }

    // public double deltaSurface(Circle other) {
    //        return Math.abs(this.surface() - other.surface());
    //    }
    public double deltaSurface(Circle other) {
        if (this.surface() > other.surface()) {
            return this.surface() - other.surface();
        } else {
            return other.surface() - this.surface();
        }
    }

    @Override
    public String toString() {
        return String.format("Color: %s\n" +
                "Radius: %d\n" +
                "Circumference: %.2f\n" +
                "Surface: %.2f\n", color, radius, circumference(), surface());
    }
}
