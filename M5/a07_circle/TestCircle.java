package a07_circle;

public class TestCircle {
    public static void main(String[] args) {
        Circle circle = new Circle(5);
        Circle small = new Circle(10, "Red");
        Circle big = new Circle(11, "Blue");

        System.out.println(circle);
        System.out.println(small);
        System.out.println(big);

        System.out.printf("Difference in circumference (red - blue): %.2f%n", small.deltaCircumference(big));
        System.out.printf("Difference in surface (blue - red): %.2f%n", big.deltaSurface(small));

    }
}
