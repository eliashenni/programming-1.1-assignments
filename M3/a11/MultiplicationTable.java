package a11;

import java.util.Scanner;

public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int num;
        do {
            System.out.print("Enter a number between 1 and 30: ");
            num = s.nextInt();
            if (num < 1 || num > 30) {
                System.out.println("Invalid number");
            }
        } while (num < 1 || num > 30);

        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= num; j++) {
                System.out.printf("|%3d", j * i);
            }
            System.out.println("|");
        }
    }
}
