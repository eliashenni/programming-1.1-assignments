package a12;

import java.util.Scanner;

public class ASCIIChristmasTree {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int width;
        int spaces;
        System.out.print("Enter the width of the Christmas Tree (uneven number between 13 and 29: ");
        width = s.nextInt();
        //checks if in range, if not then number equals lowest in range
        if (width < 13 || width > 29) {
            width = 13;
        }
        //checks if odd, if not then subtract 1
        if (width % 2 == 0) {
            width -= 1;
        }
        //for runs until i becomes greater then width, incremented by 2
        for (int i = 1; i <= width; i += 2) {
            //calculate number of spaces before stars
            spaces = (width - i) / 2;
            //prints spaces
            for (int k = 0; k < spaces; k++) {
                System.out.print(" ");
            }
            //prints stars, which is the value of i
            for (int w = 0; w < i; w++) {
                System.out.print("*");
            }
            System.out.println();
        }
        //runs 4 times because the truck is always 4 high
        for (int i = 0; i < 4; i++) {
            //calculates spaces with a hard codes 3 stars
            spaces = (width - 3) / 2;
            //prints spaces
            for (int j = 0; j < spaces; j++) {
                System.out.print(" ");
            }
            //prints stars
            for (int w = 0; w < 3; w++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
