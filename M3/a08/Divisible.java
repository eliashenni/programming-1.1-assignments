package a08;
import java.util.Scanner;

public class Divisible {
    public static void main(String[] args) {
        int firstDivisor, secondDivisor;
        int w = 0;
        boolean correctNum = true;
        try (Scanner s = new Scanner(System.in)) {

            while (true) {
                do {
                    System.out.println("If you want to quit enter '0'");
                    System.out.print("Enter the first divider: ");
                    firstDivisor = s.nextInt();
                    System.out.print("Enter the second divider: ");
                    secondDivisor = s.nextInt();
                    if (firstDivisor == 0 || secondDivisor == 0) {
                        return;
                    } else if ((firstDivisor < 0 || secondDivisor < 0)) {
                        System.out.println("Please enter positive numbers");
                    } else {
                        correctNum = false;
                    }
                } while (correctNum);

                for (int i = 1; i < 1000; i++) {
                    if (i % firstDivisor == 0 && i % secondDivisor == 0) {
                        System.out.print(i + " ");
                        w++;
                        if (w % 10 == 0) {
                            System.out.println();
                        }
                    }
                }
                System.out.println();
                System.out.println();
            }
        }
    }
}