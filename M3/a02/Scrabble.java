package a02;
import java.util.Scanner;

public class Scrabble {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.print("Enter a letter: ");
        String letter = s.next();

        switch(letter) {
            case "a": case "e": case "i":case "o":case "u":case "l":case "n":case "s": case "t": case "r":
                System.out.print("1 point"); break;
            case "d": case "g":
                System.out.print("2 points"); break;
            case "b": case "c": case "m": case "p":
                System.out.print("3 points"); break;
            case "f": case "h": case "v": case "w": case "y":
                System.out.print("4 points"); break;
            case "k":
                System.out.print("5 points"); break;
            case "j": case "x":
                System.out.print("8 points"); break;
            case "q": case "z":
                System.out.print("10 points"); break;
            default:
                System.out.print("Not a valid letter");
        }
    }
}
