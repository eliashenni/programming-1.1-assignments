package a01;
import java.util.Scanner;

public class Age {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter your age: ");
        int age = scan.nextInt();

        if(age >= 18 && age < 120) {
            System.out.print("Adult");
        } else if(age < 18 && age >= 13) {
            System.out.print("Teenager");
        } else if(age <= 12 && age >= 2) {
            System.out.print("Child");
        } else if(age <= 2 && age > 0) {
            System.out.print("Baby");
        } else {
            System.out.print("Not real age!");
        }

        if (age <= 2) {
            System.out.print("Baby");
        } else if (age <= 12) {
            System.out.print("Child");
        } else if (age <= 18) {
            System.out.print("Teenager");
        } else {
            System.out.print("Adult");
        }
    }
}
