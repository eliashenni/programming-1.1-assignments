package a09;

import java.util.Scanner;

public class Interest {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        double startingCapital, interestRate, finalCapital, doubleCapital;
        int years;
        int amountYears = 0;
        System.out.print("Enter the starting capital in €: ");
        startingCapital = s.nextDouble();
        System.out.print("Enter the interest rate in %: ");
        interestRate = s.nextDouble();
        System.out.print("Enter the number of years: ");
        years = s.nextInt();
        finalCapital = startingCapital;
        doubleCapital = startingCapital * 2;

        for (int i = 0; i < years; i++) {
            finalCapital += finalCapital * (interestRate / 100);
            amountYears++;
        }
        System.out.println("The capital will amount to €" + (long)finalCapital);

        while (finalCapital < doubleCapital) {
            finalCapital += finalCapital * (interestRate / 100);
            amountYears++;
        }
        System.out.print("It takes " + amountYears + " years to double the money.");
    }
}
