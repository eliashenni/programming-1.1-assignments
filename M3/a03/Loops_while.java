package a03;

public class Loops_while {
    public static void main(String[] args) {
        //while 1
        int i = 120;
        while (i >= 100){
            System.out.print(i-- + " ");
        }
        System.out.println();

        //while 2
        int k = 3;
        while (k < 50) {
            System.out.print(k + " ");
            k = k + 3;
        }
        System.out.println();

        //while 3
        int f = 5;
        while (f < 10_000) {
            System.out.print(f + " ");
            f = f * 5;
        }
        System.out.println();

        //while 4
        char g = 'a';
        while (g <= 'z') {
            System.out.print(g + " ");
            g++;
        }
        System.out.println();
    }
}
