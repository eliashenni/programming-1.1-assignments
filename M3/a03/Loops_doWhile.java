package a03;

public class Loops_doWhile {
    public static void main(String[] args) {
        //dowhile 1
        int i = 120;
        do {
            System.out.print(i-- + " ");
        } while (i >= 100);
        System.out.println();

        //dowhile 2
        int k = 3;
        do {
            System.out.print(k + " ");
            k = k + 3;
        } while (k < 50);
        System.out.println();

        //dowhile 3
        int f = 5;
        do {
            System.out.print(f + " ");
            f = f * 5;
        } while (f < 10_000);
        System.out.println();

        //dowhile 4
        char g = 'a';
        do {
            System.out.print(g + " ");
            g++;
        } while (g <= 'z');
        System.out.println();
    }
}
