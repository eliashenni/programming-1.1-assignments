package a03;

public class Loops_for {
    public static void main(String[] args) {
        //for1
        for (int i = 120; i >= 100; i--) {
            System.out.print(i + " ");
        }
        System.out.println();

        //for2
        for (int i = 3; i < 50; i += 3) {
            System.out.print(i + " ");
        }
        System.out.println();

        //for3
        for (int i = 5; i < 10_000; i = i * 5) {
            System.out.print(i + " ");
        }
        System.out.println();

        //for4
        for (char i = 'a'; i < 'z'; i++) {
            System.out.print(i + " ");
        }
        System.out.println();









    }
}
