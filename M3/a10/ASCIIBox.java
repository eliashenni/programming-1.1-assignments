package a10;

import java.util.Scanner;

public class ASCIIBox {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int width;
        int height;
        System.out.println("We'll draw an ASCII box using a character and dimensions of your choice.");
        System.out.print("Enter a character: ");
        char character = s.nextLine().charAt(0);
        do {
            System.out.print("Enter the width (2..60): ");
            width = s.nextInt();
            if (width < 2 || width > 60) {
                System.out.println("Invalid width.");
            }
        } while (width < 2 || width > 60);

        do {
            System.out.print("Enter the height (2..20): ");
            height = s.nextInt();
            if (height < 2 || height > 20) {
                System.out.println("Invalid height.");
            }
        } while (height < 2 || height > 20);

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print(character);
            }
            System.out.println();
        }
    }
}
