package a04;

import java.util.Scanner;

public class Multiples {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        final int MAX = 100;
        int multiple;
        int answer = 0;
        int counter = 1;
        System.out.print("Which number would you like to see the multiples of? ");
        multiple = s.nextInt();

        while (answer + multiple <= MAX) {
            answer = multiple * counter;
            counter++;
            System.out.println(answer);
        }
    }
}
