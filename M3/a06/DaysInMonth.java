package a06;

import java.util.Scanner;

public class DaysInMonth {
    public static void main(String[] args) {
        int num, year;
        String month;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter a month number (1 = January): ");
        num = s.nextInt();
        System.out.print("Enter a year (4 digits): ");
        year = s.nextInt();

        while ((num < 1 || num > 12) || (year < 1000 || year > 9999)) {
            System.out.println("Out of boundaries!");
            System.out.print("Enter a month number (1 = January): ");
            num = s.nextInt();
            System.out.print("Enter a year (4 digits): ");
            year = s.nextInt();
        }
        if ((year % 400 == 0 || (year % 4 == 0 && !(year % 100 == 0))) && num == 2) {
            month = "February has 29 days";
        } else {
            if (num == 1) {
                month = "January has 31 days";
            } else if (num == 2) {
                month = "February has 28 days";
            } else if (num == 3) {
                month = "March has 31 days";
            } else if (num == 4) {
                month = "April has 30 days";
            } else if (num == 5) {
                month = "May has 31 days";
            } else if (num == 6) {
                month = "June has 30 days";
            } else if (num == 7) {
                month = "July has 31 days";
            } else if (num == 8) {
                month = "August has 31 days";
            } else if (num == 9) {
                month = "September has 30 days";
            } else if (num == 10) {
                month = "October has 31 days";
            } else if (num == 11) {
                month = "November has 30 days";
            } else month = "December has 31 days";
        }
        System.out.println("In " + year + " " + month);
    }
}
